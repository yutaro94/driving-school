def date_converter(date):
    """Converting Japanese day to English"""
    switcher = {
        '日': 'Sunday',
        '月': 'Monday',
        '火': 'Tuesday',
        '水': 'Wednesday',
        '木': 'Thursday',
        '金': 'Friday',
        '土': 'Saturday',
    }
    return switcher.get(date, 'Invalid date format')


def time_converter(time_slot):
    """Converting time slot number to actual time"""
    switcher = {
        '1': '08:10',
        '2': '09:10',
        '3': '10:10',
        '4': '11:10',
        '5': '13:00',
        '6': '14:00',
        '7': '15:00',
        '8': '16:10',
        '9': '17:10',
        '10': '18:10'
    }
    return switcher.get(time_slot, 'Invalid time slot format')


def month_converter(month):
    """Converting month number to readable month"""
    switcher = {
        '01': 'Jan.',
        '02': 'Feb.',
        '03': 'Mar.',
        '04': 'Apr.',
        '05': 'May.',
        '06': 'Jun.',
        '07': 'Jul.',
        '08': 'Aug.',
        '09': 'Sep.',
        '10': 'Oct.',
        '11': 'Nov.',
        '12': 'Dec.'
    }
    return switcher.get(month, 'Invalid month format')


def day_converter(day):
    """Converting month number to readable month"""
    switcher = {
        '01': '1st',
        '02': '2nd',
        '03': '3rd',
        '04': '4th',
        '05': '5th',
        '06': '6th',
        '07': '7th',
        '08': '8th',
        '09': '9th',
        '10': '10th',
        '11': '11th',
        '12': '12th',
        '13': '13th',
        '14': '14th',
        '15': '15th',
        '16': '16th',
        '17': '17th',
        '18': '18th',
        '19': '19th',
        '20': '20th',
        '21': '21st',
        '22': '22nd',
        '23': '23rd',
        '24': '24th',
        '25': '25th',
        '26': '26th',
        '27': '27th',
        '28': '28th',
        '29': '29th',
        '30': '30th',
        '31': '31st'
    }
    return switcher.get(day, 'Invalid day format')
