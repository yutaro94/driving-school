from selenium import webdriver
from selenium import common
from bs4 import BeautifulSoup
from typing import Optional
from config import Config


class WebDriver:
    """
    For clicks and inputs
    """

    def __init__(self):
        options = webdriver.ChromeOptions()
        options.add_argument('--headless')
        options.add_argument('--disable-gpu')
        options.add_argument('disable-infobars')
        self.browser = webdriver.Chrome(chrome_options=options)
        self.loginUrl = Config.loginUrl
        self.userName = Config.userName
        self.password = Config.password

    def login(self) -> Optional[BeautifulSoup]:
        """
        Return None when no element is found (in maintenance).
        """
        self.browser.get(self.loginUrl)
        try:
            user_name_field = self.browser.find_element_by_css_selector("#p01aForm_b_studentId")
            password_field = self.browser.find_element_by_css_selector("#p01aForm_b_password")
            submit_button = self.browser.find_element_by_css_selector("#p01aForm_login")
        except common.exceptions.NoSuchElementException as e:
            print("In maintenance")
            self.browser.close()
            return None

        user_name_field.send_keys(self.userName)
        password_field.send_keys(self.password)
        submit_button.click()
        return self.browser.page_source

    def log_out(self) -> ():
        self.browser.close()
